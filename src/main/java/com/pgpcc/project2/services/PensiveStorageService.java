package com.pgpcc.project2.services;

public interface PensiveStorageService {
    String store(String path, String userId);
}
