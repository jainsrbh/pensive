package com.pgpcc.project2.services;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.model.*;
import com.pgpcc.project2.dtos.Memory;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class AwsPensiveAnalyticsService implements PensiveAnalyticsService {

    private final AmazonRekognition awsRekognition;
    private final String rootBucketName;
    private final Float confidenceScore;

    public AwsPensiveAnalyticsService(AmazonRekognition awsRekognition,
                                      String rootBucketName, Float confidenceScore) {
        this.awsRekognition = awsRekognition;
        this.rootBucketName = rootBucketName;
        this.confidenceScore = confidenceScore;
    }

    @Override
    public List<Memory> analyze(String memory, String userId) {
        List<Memory> memories = new ArrayList<>();
        com.amazonaws.services.rekognition.model.S3Object rekoS3Object = new com.amazonaws.services.rekognition.model.S3Object();
        rekoS3Object.setBucket(rootBucketName);
        rekoS3Object.setName(memory);
        DetectLabelsRequest request = new DetectLabelsRequest()
                .withImage(new Image()
                        .withS3Object(rekoS3Object))
                .withMaxLabels(10)
                .withMinConfidence(this.confidenceScore);

        try {
            DetectLabelsResult result = awsRekognition.detectLabels(request);
            List<Label> labels = result.getLabels();

            log.info("Detected labels for " + memory);
            labels.stream()
                    .peek(e -> log.info(e.getName() + ": " + e.getConfidence().toString()))
                    .forEach(e ->
                            memories.add(Memory.builder()
                                    .imageUrl(memory)
                                    .updateTime(LocalDateTime.now())
                                    .label(e.getName())
                                    .userId(userId)
                                    .build()));

        } catch (AmazonRekognitionException e) {
            log.error("{} exception while processing memory : {}", e.getMessage(), memory);
        }
        return memories;
    }
}
