package com.pgpcc.project2.services;

import com.pgpcc.project2.dtos.Memory;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class AwsPensiveService implements PensiveService{
    private final PensiveSearchService pensiveSearchService;
    private final PensiveAnalyticsService pensiveAnalyticsService;
    private final PensiveStorageService pensiveStorageService;

    public AwsPensiveService(PensiveSearchService pensiveSearchService,
                             PensiveAnalyticsService pensiveAnalyticsService,
                             PensiveStorageService pensiveStorageService) {
        this.pensiveSearchService = pensiveSearchService;
        this.pensiveAnalyticsService = pensiveAnalyticsService;
        this.pensiveStorageService = pensiveStorageService;
    }

    @Override
    public List<Memory> execute(String name, String userId) {
        String path = this.pensiveStorageService.store(name, userId);
        List<Memory> memories = this.pensiveAnalyticsService.analyze(path, userId);
        this.pensiveSearchService.index(memories);
        return memories;
    }
}
