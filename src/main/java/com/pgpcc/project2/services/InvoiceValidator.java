package com.pgpcc.project2.services;

import com.amazonaws.util.StringUtils;
import com.pgpcc.project2.domain.Invoice;

public class InvoiceValidator {
    public boolean isValid(Invoice invoice) {
        return !(StringUtils.isNullOrEmpty(invoice.getCustomerId()) ||
                StringUtils.isNullOrEmpty(invoice.getInvoiceId()) ||
                invoice.getTotal() == null || invoice.getAmount() == null);
    }
}
