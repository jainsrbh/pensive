package com.pgpcc.project2.services;

import com.pgpcc.project2.dtos.Memory;

import java.util.List;

public interface PensiveAnalyticsService {
    List<Memory> analyze(String memory, String userId);
}
