package com.pgpcc.project2.services;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.amazonaws.services.sns.model.UnsubscribeResult;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.UnknownHostException;

import static com.pgpcc.project2.utilities.HttpUtility.getEndPointUrl;

@Slf4j
public class SnsTopicSubscriber {
    private final AmazonSNS snsClient;
    private final String topicArn;

    private final Integer servicePort;

    public SnsTopicSubscriber(AmazonSNS snsClient, String topicArn, Integer servicePort) {
        this.topicArn = topicArn;
        this.snsClient = snsClient;
        this.servicePort = servicePort;
    }

    @PostConstruct
    public void subscribe() throws UnknownHostException {
        final String endPointUrl = getEndPointUrl("project2/s3-event", servicePort);
        log.info("Subscribe to topic: {} on http endpoint: {}", this.topicArn, endPointUrl);
        final SubscribeRequest subscribeRequest = new SubscribeRequest(this.topicArn, "http", endPointUrl);
        SubscribeResult subscribeResponse = snsClient.subscribe(subscribeRequest);
        log.info("Subscribed to arn: {}", subscribeResponse.getSubscriptionArn());
    }

    @PreDestroy
    public void unsubscribe() {
        final UnsubscribeResult unsubscribeResult = snsClient.unsubscribe(this.topicArn);
        log.info("Un-subscribed to arn: {}", unsubscribeResult.getSdkResponseMetadata().toString());
    }
}
