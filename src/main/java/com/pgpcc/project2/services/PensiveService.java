package com.pgpcc.project2.services;

import com.pgpcc.project2.dtos.Memory;

import java.util.List;

public interface PensiveService {
    List<Memory> execute(String fileName, String userId);
}
