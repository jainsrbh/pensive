package com.pgpcc.project2.services;

import com.pgpcc.project2.dtos.Memory;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;
import java.util.List;

@Slf4j
public class AwsPensiveSearchService implements PensiveSearchService {
    private final RestHighLevelClient esClient;

    public AwsPensiveSearchService(RestHighLevelClient esClient) {
        this.esClient = esClient;
    }


    @Override
    public boolean index(List<Memory> memories) {
        memories.forEach(e->{
            IndexRequest request = new IndexRequest("memory", "_doc",
                    String.valueOf(e.getUpdateTime().hashCode())).source(e, XContentType.JSON);
            IndexResponse response = null;
            try {
                response = esClient.index(request, RequestOptions.DEFAULT);
                log.info(response.toString());
            } catch (Exception ex) {
                log.error("{} exception while processing memory : [{}]",ex.getMessage(), e);
            }
        });
        return true;
    }
}
