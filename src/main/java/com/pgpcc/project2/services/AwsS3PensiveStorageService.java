package com.pgpcc.project2.services;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectResult;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
public class AwsS3PensiveStorageService implements PensiveStorageService {
    private final AmazonS3 amazonS3;
    private final String rootBucketName;

    public AwsS3PensiveStorageService(AmazonS3 amazonS3,
                                      String rootBucketName) {
        this.amazonS3 = amazonS3;
        this.rootBucketName = rootBucketName;
    }

    @Override
    public String store(String fileName, String userId){
        log.info("New memory: [{}]",fileName);
        File file = new File(fileName);
        String s3Path = userId + "/" + file.getName();
        PutObjectResult result = amazonS3.putObject(rootBucketName, s3Path, file);
        log.info("{} file name uploaded to S3 for user {} at path : {}", fileName, userId, s3Path);
        return s3Path;
    }

}
