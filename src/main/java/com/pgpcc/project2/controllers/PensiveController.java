package com.pgpcc.project2.controllers;

import com.pgpcc.project2.dtos.Memory;
import com.pgpcc.project2.dtos.Message;
import com.pgpcc.project2.services.PensiveAnalyticsService;
import com.pgpcc.project2.services.PensiveService;
import com.pgpcc.project2.services.PensiveStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/pensive")
@Api(value = "pensive", produces = "application/json" )
@Slf4j
public class PensiveController {
    private final String version;
    private final PensiveStorageService pensiveStorageService;
    private final PensiveAnalyticsService pensiveAnalyticsService;
    private final PensiveService pensiveService;

    @Autowired
    public PensiveController(@Value("${service.version}") String version,
                             PensiveStorageService pensiveStorageService,
                             PensiveAnalyticsService pensiveAnalyticsService,
                             PensiveService pensiveService) {
        this.version = version;
        this.pensiveStorageService = pensiveStorageService;
        this.pensiveAnalyticsService = pensiveAnalyticsService;
        this.pensiveService = pensiveService;
    }

    @RequestMapping(value = "/version", method = RequestMethod.GET)
    @ApiResponse(code = 200, message = "Success", response = Message.class)
    public ResponseEntity<Message> version() {
        return new ResponseEntity(new Message(version), HttpStatus.OK);
    }

    @RequestMapping(value = "/memory", method = RequestMethod.POST)
    @ApiResponse(code = 200, message = "Success", response = Memory.class)
    public ResponseEntity<Message> memory(@RequestParam("image") @NotNull MultipartFile file){
        byte[] bytes = new byte[0];
        try {
            bytes = file.getBytes();
            Path path = Paths.get(file.getOriginalFilename());
            Files.write(path, bytes);
            String userId = "saurabhjn@gmail.com";
            List<Memory> memories = this.pensiveService.execute(file.getOriginalFilename(), userId);
            return new ResponseEntity(memories, HttpStatus.OK);
        } catch (Exception e) {
            log.error("{} exception while processing file {}",e.getMessage(),file.getName());
            return new ResponseEntity(new Message("Server error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
