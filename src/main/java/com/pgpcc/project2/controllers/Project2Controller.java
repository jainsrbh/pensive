package com.pgpcc.project2.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pgpcc.project2.aws.message.SNSNotification;
import com.pgpcc.project2.aws.message.sns.Records;
import com.pgpcc.project2.domain.Invoice;
import com.pgpcc.project2.dtos.Message;
import com.pgpcc.project2.services.S3InvoiceService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project2")
@Api(value = "project2")
@Slf4j
public class Project2Controller {
    private final S3InvoiceService s3InvoiceService;
    private final ObjectMapper objectMapper;

    @Autowired
    public Project2Controller(S3InvoiceService s3InvoiceService, ObjectMapper objectMapper) {
        this.s3InvoiceService = s3InvoiceService;
        this.objectMapper = objectMapper;
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Message> newFileSnsEvent() {
        return new ResponseEntity(new Message("project2"), HttpStatus.OK);
    }

    @RequestMapping(value = "/s3-event", method = RequestMethod.POST, produces = "application/json", consumes = "text/plain;charset=UTF-8")
    public ResponseEntity<?> newFileSnsEvent(@RequestHeader("x-amz-sns-message-type") String messageType,
                                             @RequestBody String message) {
        log.info("s3-event , messageType : {}",messageType);
        log.info("s3-event , message : {}",message);
        try{
            if ("Notification".equals(messageType)) {
                SNSNotification notification = this.objectMapper.readerFor(SNSNotification.class).readValue(message);
                List<Invoice> invoices = this.s3InvoiceService.processInvoice(parseNotificationMessage(notification));
                return new ResponseEntity(invoices, HttpStatus.OK);
            }else{
                log.info("Unknown notification type : {}, return ok response",messageType);
                return ResponseEntity.ok().build();
            }
        }catch(Exception e){
            log.error(e.getMessage(),e);
            return new ResponseEntity(new com.pgpcc.project2.dtos.Message(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/s3-file", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Invoice> logS3Invoice(@RequestParam(value = "bucketName") String bucketName,
                                                @RequestParam(value = "keyName") String keyName) {
        Invoice invoice = this.s3InvoiceService.processInvoice(bucketName, keyName);
        return new ResponseEntity(invoice, HttpStatus.OK);
    }

    private Records parseNotificationMessage(SNSNotification snsNotification) throws JsonProcessingException {
        String m = snsNotification.getMessage();
        String r = m.replaceAll("\\\\","");
        log.info(r);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readerFor(Records.class).readValue(r);
    }
}
