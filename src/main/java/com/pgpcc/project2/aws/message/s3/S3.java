package com.pgpcc.project2.aws.message.s3;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "s3SchemaVersion",
        "configurationId",
        "bucket",
        "object"
})
@Data
public class S3 implements Serializable {

    private final static long serialVersionUID = 2884129881006092339L;
    @JsonProperty("s3SchemaVersion")
    public String s3SchemaVersion;
    @JsonProperty("configurationId")
    public String configurationId;
    @JsonProperty("bucket")
    public Bucket bucket;
    @JsonProperty("object")
    public S3Object object;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
