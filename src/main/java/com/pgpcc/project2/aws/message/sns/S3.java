
package com.pgpcc.project2.aws.message.sns;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "s3SchemaVersion",
    "configurationId",
    "bucket",
    "object"
})
@Data
public class S3 implements Serializable
{

    @JsonProperty("s3SchemaVersion")
    public String s3SchemaVersion;
    @JsonProperty("configurationId")
    public String configurationId;
    @JsonProperty("bucket")
    public Bucket bucket;
    @JsonProperty("object")
    public SnsObject object;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();
    private final static long serialVersionUID = 6802379436713514633L;

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
