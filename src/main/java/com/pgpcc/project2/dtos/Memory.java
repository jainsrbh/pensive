package com.pgpcc.project2.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Memory implements Serializable {
    private String userId;
    private String label;
    private String imageUrl;
    private LocalDateTime updateTime;
}
