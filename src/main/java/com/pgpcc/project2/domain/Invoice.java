package com.pgpcc.project2.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "invoice")
public class Invoice implements Serializable {
    @DynamoDBAttribute
    private String customerId;
    @DynamoDBHashKey
    private String invoiceId;
    @DynamoDBAttribute
    private Date date;
    @DynamoDBAttribute
    private String from;
    @DynamoDBAttribute
    private String to;
    @DynamoDBAttribute
    private BigDecimal amount;
    @DynamoDBAttribute
    private BigDecimal sgst;
    @DynamoDBAttribute
    private BigDecimal total;
    @DynamoDBAttribute
    private String totalInWords;
    @DynamoDBAttribute
    private List<String> items;

    public String csv() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return new StringBuilder()
                .append(customerId).append(",")
                .append(invoiceId).append(",")
                .append(sdf.format(this.date)).append(",")
                .append(from).append(",")
                .append(to).append(",")
                .append(amount).append(",")
                .append(sgst).append(",")
                .append(total).append(",")
                .append(totalInWords).toString();
    }


}
