package com.pgpcc.project2.config;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.pgpcc.project2.repositories.InvoiceRepository;
import com.pgpcc.project2.services.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsS3Config {
    @Bean
    public AmazonS3 s3Client(@Value("${aws.region}") String region) {
        return AmazonS3ClientBuilder.standard().withRegion(region).build();
    }

    @Bean
    public S3InvoiceService s3InvoiceService(AmazonS3 s3Client, InvoiceRepository invoiceRepository) {
        return new S3InvoiceService(s3Client, new InvoiceFileParser(),
                invoiceRepository, new InvoiceCsvWriter(), new InvoiceValidator());
    }

    @Bean
    public PensiveStorageService s3PensiveService(AmazonS3 s3Client,
                                                       @Value("${aws.s3.root.bucket}") String rootBucketName){
        return new AwsS3PensiveStorageService(s3Client, rootBucketName);
    }
}
