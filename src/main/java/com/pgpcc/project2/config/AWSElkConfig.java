package com.pgpcc.project2.config;

import com.amazonaws.auth.AWS4Signer;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.pgpcc.project2.services.AWSRequestSigningApacheInterceptor;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequestInterceptor;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

public class AWSElkConfig {

    @Bean
    public RestHighLevelClient esClient(AWSCredentialsProvider awsCredentialsProvider,
                                        @Value("${aws.elk.service.name}") String serviceName,
                                        @Value("${aws.region}") String region,
                                        @Value("${aws.elk.endpoint}") String aesEndpoint) {
        AWS4Signer signer = new AWS4Signer();
        signer.setServiceName(serviceName);
        signer.setRegionName(region);
        HttpRequestInterceptor interceptor = new AWSRequestSigningApacheInterceptor(serviceName, signer, awsCredentialsProvider);
        return new RestHighLevelClient(RestClient.builder(HttpHost.create(aesEndpoint))
                .setHttpClientConfigCallback(hacb -> hacb.addInterceptorLast(interceptor)));
    }
}
