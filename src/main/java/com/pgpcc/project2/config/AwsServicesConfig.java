package com.pgpcc.project2.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.pgpcc.project2.services.*;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsServicesConfig {
    @Bean
    public AmazonRekognition getRekognitionClient() {
        return AmazonRekognitionClientBuilder.defaultClient();
    }

    @Bean
    public PensiveAnalyticsService pensiveAnalyticsService(
            AmazonRekognition amazonRekognition,
            @Value("${aws.s3.root.bucket}") String rootBucketName,
            @Value("${aws.reko.confidence.score:90}") String confidenceScore) {
        return new AwsPensiveAnalyticsService(amazonRekognition, rootBucketName,
                new Float(confidenceScore));
    }

    @Bean
    public PensiveService pensiveService(PensiveAnalyticsService pensiveAnalyticsService,
                                         PensiveStorageService pensiveStorageService,
                                         RestHighLevelClient esClient){
        return new AwsPensiveService(
                new AwsPensiveSearchService(esClient),
                pensiveAnalyticsService, pensiveStorageService );
    }
}
