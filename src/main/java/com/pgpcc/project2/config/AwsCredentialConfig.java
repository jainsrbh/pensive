package com.pgpcc.project2.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsCredentialConfig {
    @Bean
    public AWSCredentialsProvider awsCredentialsProvider(@Value("${aws.creds.file}") String credsFile) {
        return new ProfileCredentialsProvider(credsFile, "default");
    }
}
